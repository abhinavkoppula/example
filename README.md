## Verification guide
### From command line
- Ensure you have Java 17 installed
- `mvn clean compile install` to build and also run the unit-tests and generate the coverage report
- Jacoco is used to generate the coverage report. Coverage report can be verified under `target/site/jacoco/index.html`
- `mvn test` is used to only run the unit-tests

### From Gitlab CI
- Gitlab pipelines can be checked here for this project - https://gitlab.com/abhinavkoppula/example/-/pipelines
- Here 3 variables are configured in Gitlab project variables. To configure these variables goto the Gitlab project, Settings > CI/CD > Variables
- SONAR_HOST_URL: https://sonarcloud.io  
- SONAR_ORG: `organisation name as seen in the SonarCloud dashboard`
- SONAR_TOKEN: `Sonar token as obtained from SonarCloud`

### Gitlab CI file
- 3 stages are configured, clean, build and test
- Clean stage is used to clean leftovers from previous builds
- Build stage builds the project
- Test stage comprises of 2 steps, sonarcloud-check and unit-test
- sonarcloud-check runs quality checks via SonarCloud
- unit-test runs unit-tests and generates coverage report


package com.topcoder.example;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ExampleApplicationTests {

	@Test
	void contextLoads() {
    ExampleApplication.main(new String[] {});
	}

}

package com.topcoder.example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SampleTest {

  @Test
  public void testCalculate() {
    assertEquals(new Sample().add(4, 5), 9);
  }
}
